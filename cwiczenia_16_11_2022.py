import numpy as np

#zadanie 1
# tekst = "to jest testowe zdanie"
# print(len(tekst))
# print(tekst[12])

#zadanie 2 
# listaimion = ["Jan", "Ola"]
# imie = input("Podaj swoje imie:")
# listaimion.append(imie)
# print(listaimion)

#zadanie 3
# listaimion1 = ["Jan", "Ola", "Agnieszka",3,5,"Kasia","Basia"]
# print(listaimion1[2])
# print(len(listaimion1))

# print(listaimion1[1:]) #jak tak zostawimy to sie wyswietli do konca od 2 elementu
# print(listaimion1[:3]) #od poczatku do elementu 4
# print(listaimion1[1:3]) #przedzial od do

#zadanie 4

lista = [2,3,5,6,4,8,11,43,23]

tablica = np.array(lista)

print(lista)

print(tablica)

max = tablica.max()

srednia = tablica.mean()
print(srednia)

# print(f"wartosc max to: {max}")

# print(f"wartosc srednia to: {srednia}")

# kopiaTablicy = tablica*10

# print(kopiaTablicy)

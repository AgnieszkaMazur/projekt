#zadanie 1
# a = 3.7
# print(type(a))
# print(isinstance(a,int))
# print(isinstance(a,float))

# if(isinstance(a,float)):
#     print("Zmienna a ma wartosci rzeczywiste")
# else:
#     print("Zmienna a ma wartosci całkowite ")


#zadanie 2
import turtle

# bok = 200
# predkosc = 1

# zolw = turtle.Turtle()
# zolw.speed(1)
# zolw.forward(bok)
# zolw.left(90)
# zolw.forward(bok)
# zolw.left(90)
# zolw.forward(bok)
# zolw.left(90)
# zolw.forward(bok)
# zolw.right(90)
# zolw.forward(bok)
# zolw.left(90)
# zolw.forward(bok)
# zolw.left(90)
# zolw.forward(bok)
# zolw.left(90)
# zolw.forward(bok)
# turtle.exitonclick()

#zadanie 3 
turtle.setworldcoordinates(-400, 0, 400, 800)
import math
szerokosc_domu = 600
wysokosc_parteru = 200
szer_drzwi = 50
wys_drzwi = 90
dlugosc_dachu = (szerokosc_domu/math.sqrt(2))

zolw = turtle.Turtle()
zolw.forward((szerokosc_domu/2)-(szer_drzwi/2))
zolw.left(90)
zolw.forward(wysokosc_parteru)
zolw.left(90)
zolw.forward(szerokosc_domu)
zolw.left(90)
zolw.forward(wysokosc_parteru)
zolw.left(90)
zolw.forward((szerokosc_domu/2)+szer_drzwi)
zolw.left(90)
zolw.forward(wys_drzwi)
zolw.left(90)
zolw.forward(szer_drzwi)
zolw.left(90)
zolw.forward(wys_drzwi)
zolw.right(90)
zolw.forward(szerokosc_domu/2)
zolw.right(90)
zolw.forward(wysokosc_parteru)
zolw.right(45)
zolw.forward(dlugosc_dachu)
zolw.right(90)
zolw.forward(dlugosc_dachu)
turtle.exitonclick()



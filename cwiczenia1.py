#cwiczenie1

dzien = 8
miesiac = 8
suma = dzien + miesiac
print(suma)

#cwiczenie2
dzien = 8
miesiac = int(input("Podaj miesiac:"))
suma = dzien + miesiac
print(type(miesiac))
print(suma)

#cwiczenie3
a = ["Ola", "Ala", "Iza"]
b = "Zajecia z programowania"
c = 10.23
d = 11
print(type(a))
print(type(b))
print(type(c))
print(type(d))

#cwiczenie4
a=9
b=8
if(a>b):
   print("a jest wieksze od b")
else:
   print("b jest wieksze od a")

#cwiczenie5
a = ["Ola", "Ala", "Iza"]
for elementList in a:
   print(elementList)

#cwiczenie 6
with open("plik1.txt") as plik:
    for wiersz in plik:
        print(wiersz)

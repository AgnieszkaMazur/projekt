import numpy as np
import csv
import os

# folder="C:\\Users\\agap8\\OneDrive\\Pulpit\\studiamgr_n\\programowanie\\guided_practice"

# lista=[]
# pliki=os.listdir(folder)

# for i in pliki:
#     with open((os.path.join(folder,i)),"r",encoding="utf8") as plikcsv:
#         zmienna=csv.reader(plikcsv)
#         next(zmienna)
#         for row in zmienna:
#             lista.append(row)
            

# tablica=np.array(lista)
# id_stacji=tablica[:,0]
# stacja=tablica[:,1]
# godzina=tablica[:,3]
# temperatura=tablica[:,4]
# opad=tablica[:,8]

# temp_min=float(temperatura[0])
# temp_min_czas=None
# temp_min_stacja=None

# temp_max=float(temperatura[0])
# temp_max_czas=None
# temp_max_stacja=None

# for i in range(len(stacja)):
#     if float(temperatura[i]) < float(temp_min):
#         temp_min=temperatura[i]
#         temp_min_czas=godzina[i]
#         temp_min_stacja=stacja[i]
#     if float(temperatura[i]) > float(temp_max):
#         temp_max=temperatura[i]
#         temp_max_czas=godzina[i]
#         temp_max_stacja=stacja[i]


# temp_min_wynik="Najniższą temperaturę zanotowano na stacji "+temp_min_stacja+" o godzinie "+temp_min_czas+", wynosiła ona: "+temp_min+" °C"
# temp_max_wynik="Najwyższą temperaturę zanotowano na stacji "+temp_max_stacja+" o godzinie "+temp_max_czas+", wynosiła ona: "+temp_max+" °C"
    
# print(temp_min_wynik)
# print(temp_max_wynik)
# print("Opad atmosferyczny wystąpił na stacjach:")

# for i in range(len(stacja)):
#     if float(opad[i]) > float(0.0):
#         print(stacja[i]+" (Godzina: "+godzina[i]+")")

#zadanie 2 
folder_2 = "C:\\Users\\agap8\\OneDrive\\Pulpit\\studiamgr_n\\programowanie\\pliki"

lista=[]
pliki=os.listdir(folder_2)

for i in pliki:
    with open((os.path.join(folder_2,i)),"r",encoding="utf8") as plikcsv:
        zmienna=csv.reader(plikcsv)
        next(zmienna)
        for row in zmienna:
            lista.append(row)
            

tablica=np.array(lista)
id_stacji=tablica[:,0]
stacja=tablica[:,1]
godzina=tablica[:,3]
temperatura=tablica[:,4]
opad=tablica[:,8]

temp_min=float(temperatura[0])
temp_min_czas=None
temp_min_stacja=None

temp_max=float(temperatura[0])
temp_max_czas=None
temp_max_stacja=None

for i in range(len(stacja)):
    if float(temperatura[i]) < float(temp_min):
        temp_min=temperatura[i]
        temp_min_czas=godzina[i]
        temp_min_stacja=stacja[i]
    if float(temperatura[i]) > float(temp_max):
        temp_max=temperatura[i]
        temp_max_czas=godzina[i]
        temp_max_stacja=stacja[i]


temp_min_wynik="Najniższą temperaturę zanotowano na stacji "+temp_min_stacja+" o godzinie "+temp_min_czas+", wynosiła ona: "+temp_min+" °C"
temp_max_wynik="Najwyższą temperaturę zanotowano na stacji "+temp_max_stacja+" o godzinie "+temp_max_czas+", wynosiła ona: "+temp_max+" °C"
    
print(temp_min_wynik)
print(temp_max_wynik)
print("Opad atmosferyczny wystąpił na stacjach:")

for i in range(len(stacja)):
    if float(opad[i]) > float(0.0):
        print(stacja[i]+" (Godzina: "+godzina[i]+")")
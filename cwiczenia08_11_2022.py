
#zadanie 1
# imie = "Agnieszka"
# nazwisko = "Mazur"
# print("Czesc %s. Nazywam sie %s"%(imie,nazwisko))
# #print("Czesc {}. Nazywam sie {}."format(imie,nazwisko))
# print(f"Czesc {imie}. Nazywam sie {nazwisko}.")

#zadanie 2
import math 
# pi = math.pi
# print(pi)
# print("%.3f"%pi) #pi z dokladnoscia do 3 msc po przecinku
# print("{:.3f}".format(pi))
# print(f"{pi:.3f}")

#zadanie 3
a = 3
b = 4.5
c = a+b
print(type(c))
print(isinstance(c,float))
print(isinstance(c,int))

#zadanie 4
"Zmienna c jest typu int"
"Zmienna c nie jest typu int"
if(isinstance(c,float)):
    print(f"Zmienna c jest typu int")
else:
    print(f"Zmienna c nie jest typu int")



#zadanie 5

# import turtle

# zolw = turtle.Turtle()
# zolw.shape('turtle') #zmienia kształt na zolwia


# zolw.forward(100)
# zolw.right(90)
# zolw.forward(100)
# zolw.right(90)
# zolw.forward(100)
# zolw.right(90)
# zolw.forward(100)

# for i in range(10):
#     zolw.circle(i*10) #zolw rysuje kolo kazde o 10 wieksze (zaczyna od promienia 0)

# turtle.exitonclick() #okno sie nie zamyka